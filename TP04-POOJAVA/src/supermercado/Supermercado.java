package supermercado;

import java.util.ArrayList;

public class Supermercado {
	private String nombre, direccion;
	private ArrayList<Producto> productos = new ArrayList<Producto>();
	
	public Supermercado(String nombre, String direccion) {
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	public void agregarProducto(Producto producto) {
		productos.add(producto);
	}
	
	public Double getPrecioTotal() {
		Double precioTotal = 0.00;
		for (int i = 0; i < productos.size(); i++) {
			if (productos.get(i).esPrecioCuidado()) {
				precioTotal += productos.get(i).getPrecio() * productos.get(i).getDescuento();
			} else {
				precioTotal += productos.get(i).getPrecio();
			}
		}
		return precioTotal;
	}
	
	public int getCantidadDeProductos() {
		return productos.size();
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getDireccion() {
		return this.direccion;
	}
}
