package supermercado;

public class Producto {
	private String nombre;
	private Double precio;
	private boolean esPrecioCuidado;
	private Double descuento;
	
	public Producto(String nombre, Double precio, boolean esPrecioCuidado, Double descuento) {
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = esPrecioCuidado;
		this.descuento = descuento;
	}
	
	public Producto(String nombre, Double precio) {
		this.nombre = nombre;
		this.precio = precio;
		this.esPrecioCuidado = false;
		//this.descuento = 0d;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Double getPrecio() {
		return this.precio;
	} 
	
	public boolean esPrecioCuidado() {
		return this.esPrecioCuidado;
	}
	
	public void aumentarPrecio(Double precio) {
		this.precio += precio;
	}
	
	public Double getDescuento() {
		return this.descuento;
	}
}
