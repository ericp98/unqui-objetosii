package trabajador;

public class IngresoPorHorasExtra extends Ingreso {
	
	public IngresoPorHorasExtra(String mes, String concepto, Double monto) {
		super(mes, concepto, monto);
	}

	@Override
	public Double getMontoImponible() {
		return 0d;
	}
}
