package trabajador;

import java.util.ArrayList;

public class Trabajador {
	private ArrayList<Ingreso> ingresos = new ArrayList<Ingreso>();
	
	public void addIngreso(Ingreso ingreso) {
		ingresos.add(ingreso);
	}
	
	public ArrayList<Ingreso> getIngresos() {
		return this.ingresos;
	}
	
	public Double getTotalPercibido() {
		Double total = 0d;
		for (int i = 0; i < ingresos.size(); i++) {
			total += ingresos.get(i).getMonto();
		}
		return total;
	}
	
	public Double getMontoImponible() {
		Double montoImponible = 0d;
		for (int i = 0; i < ingresos.size(); i++) {
			montoImponible += ingresos.get(i).getMontoImponible();
		}
		return montoImponible;
	}
	
	public Double getImpuestoAPagar() {
		return this.getMontoImponible() * 0.02;
	}
	
}
