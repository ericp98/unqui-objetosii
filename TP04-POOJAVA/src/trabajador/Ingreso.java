package trabajador;

public abstract class Ingreso {
	private String mesDePercepcion, concepto;
	private Double montoPercibido;
	
	public Ingreso(String mes, String concepto, Double monto) {
		this.mesDePercepcion = mes;
		this.concepto = concepto;
		this.montoPercibido = monto;
	}
	
	public String getMes() {
		return this.mesDePercepcion;
	}
	
	public String getConcepto() {
		return this.concepto;
	}
	
	public Double getMonto() {
		return this.montoPercibido;
	}
	
	public abstract Double getMontoImponible();
}
