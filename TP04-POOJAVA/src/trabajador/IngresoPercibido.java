package trabajador;

public class IngresoPercibido extends Ingreso {

	public IngresoPercibido(String mes, String concepto, Double monto) {
		super(mes, concepto, monto);
	}
	
	@Override
	public Double getMontoImponible() {
		return this.getMonto();
	}
	
}
