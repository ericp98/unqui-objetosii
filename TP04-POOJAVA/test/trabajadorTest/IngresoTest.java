package trabajadorTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

// Packages
import trabajador.IngresoPercibido;
import trabajador.IngresoPorHorasExtra;

public class IngresoTest {
	private IngresoPorHorasExtra ingresoHE;
	private IngresoPercibido ingresoP;

	@Test
	public void testConstructor() {
		ingresoHE = new IngresoPorHorasExtra("Enero", "Sueldo", 1000.00);
		String mes = ingresoHE.getMes();
		String concepto = ingresoHE.getConcepto();
		Double monto = ingresoHE.getMonto();
		assertEquals("Enero", mes);
		assertEquals("Sueldo", concepto);
		assertEquals(1000.00, monto);
	}
	
	@Test
	public void testMontoImponible() {
		ingresoHE = new IngresoPorHorasExtra("Enero", "Sueldo", 1000.00);
		ingresoP = new IngresoPercibido("Enero", "Sueldo", 200.00);
		Double montoHE = ingresoHE.getMontoImponible();
		Double montoP = ingresoP.getMontoImponible();
		assertEquals(0, montoHE);
		assertEquals(200.00, montoP);
	}

}
