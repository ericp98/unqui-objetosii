package trabajadorTest;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

// Packages
import trabajador.Trabajador;
import trabajador.IngresoPorHorasExtra;
import trabajador.IngresoPercibido;

public class TrabajadorTest {
	private Trabajador trabajador;
	private IngresoPorHorasExtra ingresoHE;
	private IngresoPercibido ingresoP;
	
	@Before
	public void setUp() {
		ingresoHE = new IngresoPorHorasExtra("Enero","Sueldo",1000d);
		ingresoP = new IngresoPercibido("Enero", "Horas extra", 200d);
		trabajador = new Trabajador();
		
//		trabajador.addIngreso(ingresoHE);
//		trabajador.addIngreso(ingresoP);
	}
	
	@Test
	public void sizetest() {
		int ingresos = trabajador.getIngresos().size();
		assertEquals(0, ingresos);
	}

//	@Test
//	public void getTotalPercibidoTest() {
//		Double total = trabajador.getTotalPercibido();
//		assertEquals(1200d, total);
//	}
//	
//	@Test
//	public void getMontoImponibleTest() {
//		Double montoImponible = trabajador.getMontoImponible();
//		assertEquals(200d, montoImponible);
//	}
//	
//	@Test
//	public void getImpuestoAPagarTest() {
//		Double montoImpuestos = trabajador.getImpuestoAPagar();
//		assertEquals(24d, montoImpuestos);
//	}

}
