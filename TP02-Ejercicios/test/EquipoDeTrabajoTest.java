import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import persona.EquipoDeTrabajo;
import persona.Persona;

import java.util.ArrayList;

public class EquipoDeTrabajoTest {
	private EquipoDeTrabajo equipo;
	
	@Before
	public void setUp() throws Exception {
		Persona persona = new Persona("Nicolas", "Lopez", 20);
		Persona persona2 = new Persona("Diego", "Perez", 35);
		Persona persona3 = new Persona("Romina", "Fernandez", 45);
		Persona persona4 = new Persona("Lucas", "Gonzalez", 32);
		Persona persona5 = new Persona("Martin", "Benitez", 34);
		
		ArrayList<Persona> personas = new ArrayList<Persona>();
		personas.add(persona);
		personas.add(persona2);
		personas.add(persona3);
		personas.add(persona4);
		personas.add(persona5);
		
		equipo = new EquipoDeTrabajo("equipo1", personas);
		
	}

	@Test
	public void testPromedioEdad() {
		int promedio = equipo.getPromedioEdad();
		assertEquals(promedio, 33);
	}
	
	@Test
	public void testNombre() {
		String nombre = equipo.getNombre();
		assertEquals(nombre, "equipo1");
	}
}
