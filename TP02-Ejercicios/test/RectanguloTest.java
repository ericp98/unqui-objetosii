import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


import rectangulo.Rectangulo;
import point.Point;

public class RectanguloTest {
	private Point esquinaInferiorIzq;
	private Rectangulo rec;
	
	@Before
	public void setUp() throws Exception {
		esquinaInferiorIzq = new Point(1,1);
		rec = new Rectangulo(esquinaInferiorIzq, 3, 3);	
	}

	@Test
	public void construccionRectangulo() {
		Point esquinaIzq = rec.getEsquinaInferiorIzq();
		Point esquinaDer = rec.getEsquinaInferiorDer();
		Point esquinaSupDer = rec.getEsquinaSuperiorDer();
		Point esquinaSupIzq = rec.getEsquinaSuperiorIzq();
		assertEquals(esquinaIzq, esquinaInferiorIzq);
		assertEquals(esquinaDer.getPointX(), 4);
		assertEquals(esquinaSupDer.getPointY(), 4);
		assertEquals(esquinaSupIzq.getPointY(), 4);
	}
	
	@Test
	public void areaTest() {
		int area = rec.getArea();
		assertEquals(area, 9);
	}
	
	@Test
	public void perimetroTest() {
		int perimetro = rec.getPerimetro();
		assertEquals(perimetro, 12);
	}
	
	@Test
	public void sentidoTest() {
		assertEquals(false, rec.isHorizontal()); 
	}
	
	@Test
	public void isCuadradoTest() {
		assertEquals(true, rec.isCuadrado()); 
	}
	
}
