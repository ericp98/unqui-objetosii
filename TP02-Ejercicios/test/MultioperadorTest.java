import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import multiOp.Multioperador;

public class MultioperadorTest {
	private Multioperador multi;

	@Before
	public void setUp() throws Exception {
		multi = new Multioperador();
		
		multi.addNumber(3);
		multi.addNumber(2);
		multi.addNumber(1);
		
	}
	
	@Test
	public void testSuma() {
		int suma = multi.sumar();
		assertEquals(suma, 6);
	}
	
	@Test
	public void testResta() {
		int resta = multi.restar();
		assertEquals(resta, 0);
	}
	
	@Test
	public void testMultiplicacion() {
		int multiplicar = multi.multiplicar();
		assertEquals(multiplicar, 6);
	}
}
