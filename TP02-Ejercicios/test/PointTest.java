import static org.junit.Assert.*;
import org.junit.Test;


import point.Point;

public class PointTest {
	
	@Test
	public void testPointVacio() {
		Point point = new Point();
		int x = point.getPointX();
		int y = point.getPointY();
		assertEquals(x, 0);
		assertEquals(y, 0);
	}
	
	@Test
	public void testPointConValores() {
		Point point = new Point(1,2);
		int x = point.getPointX();
		int y = point.getPointY();
		assertEquals(x, 1);
		assertEquals(y, 2);
	}
	
	@Test
	public void testMoveTo() {
		Point point = new Point();
		point.moveTo(5, 5);
		int x = point.getPointX();
		int y = point.getPointY();
		assertEquals(x, 5);
		assertEquals(y, 5);
	}
	
	@Test
	public void testAddPoint() {
		Point point = new Point(1,1);
		Point point2 = new Point(2,2);
		Point pointAdd = point.addPoint(point2);
		assertEquals(pointAdd.getPointX(), 3);
		assertEquals(pointAdd.getPointY(), 3);
	}
}
