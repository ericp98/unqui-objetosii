package multiOp;

import java.util.ArrayList;

public class Multioperador {
	private ArrayList<Integer> enteros = new ArrayList<Integer>();
	
	public void addNumber(int n) {
		enteros.add(n);
	}
	
	public int sumar() {
		int suma = 0;
		for(int i = 0; i < enteros.size(); i++) {
			suma += enteros.get(i);
		}
		return suma;
	}
	
	public int restar() {
		int resta = enteros.get(0);
		for (int i = 1; i < enteros.size(); i++) {
			resta -= enteros.get(i);
		}
		return resta;
	}
	
	public int multiplicar() {
		int acumulador = 1;
		for(int i=0; i < enteros.size(); i++) {
			acumulador *= enteros.get(i);
		}
		return acumulador;
	}
}
