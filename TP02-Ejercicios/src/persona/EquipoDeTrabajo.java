package persona;
import java.util.ArrayList;

public class EquipoDeTrabajo {
	private String nombre; 
	ArrayList<Persona> personas = new ArrayList<Persona>();
	
	public EquipoDeTrabajo(String nombre, ArrayList<Persona> personas) {
		this.nombre = nombre;
		this.personas = personas;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public int getPromedioEdad() {
		int sumaEdades = 0;
		for (int i = 0; i < personas.size(); i++) {
			sumaEdades += personas.get(i).getEdad();
		}
		return sumaEdades / personas.size();
	}
}
