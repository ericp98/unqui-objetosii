package rectangulo;
import point.Point;

public class Rectangulo {
	private Point esquinaInferiorIzq, esquinaInferiorDer, esquinaSuperiorDer, esquinaSuperiorIzq;
	private int base, altura;
	
	public Rectangulo(Point esquinaInferiorIzq, int alto, int ancho) {
		this.esquinaInferiorIzq = esquinaInferiorIzq;
		this.esquinaInferiorDer = esquinaInferiorIzq.addPoint(new Point(ancho, 0));
		this.esquinaSuperiorIzq = esquinaInferiorIzq.addPoint(new Point(0, alto));
		this.esquinaSuperiorDer = esquinaInferiorDer.addPoint(new Point(0, alto));
		this.base = ancho;
		this.altura = alto;
	}
	
	public int getArea() {
		return this.base * this.altura;
	}
	
	public int getPerimetro() {
		return (this.base * 2) + (this.altura * 2);
	}
	
	public boolean isHorizontal() {
		return this.base > this.altura;
	}
	
	public boolean isCuadrado() {
		return this.base == this.altura;
	}
	
	public Point getEsquinaInferiorDer() {
		return this.esquinaInferiorDer;
	}	
	
	public Point getEsquinaInferiorIzq() {
		return this.esquinaInferiorIzq;
	}	
	
	public Point getEsquinaSuperiorDer() {
		return this.esquinaSuperiorDer;
	}	
	
	public Point getEsquinaSuperiorIzq() {
		return this.esquinaSuperiorIzq;
	}	
}
