package unq;

import java.util.ArrayList;

public class Counter {

	private ArrayList<Integer> arrayNums = new ArrayList<Integer>();
	
	public void addNumber(int n) {
		arrayNums.add(n);
	};
		
	public int getPares() {
		int pares = 0;
		for (int i = 0; i < arrayNums.size(); i++) {
			if (arrayNums.get(i) % 2 == 0) {
				pares += 1;
			}
		}
		return pares;
	};
	
	public int getImpares() {
		int impares = 0;
		for (int i = 0; i < arrayNums.size(); i++) {
			if (arrayNums.get(i) % 2 != 0) {
				impares += 1;
			}
		}
		return impares;
	};
	
	public int getMultiplosDe(int n) {
		int cantidadMultiplos = 0;
		for (int i = 0; i < arrayNums.size(); i++) {
			if (arrayNums.get(i) % n == 0) {
				cantidadMultiplos += 1;
			}
		}
		return cantidadMultiplos;
	};
}
