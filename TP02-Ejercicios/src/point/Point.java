package point;

public class Point {
	private int x = 0, y = 0;		
	
	// Constructor vacio, deja las variables en 0
	public Point() {
		
	}
	
	// Constructor con parametros
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void moveTo(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Point addPoint(Point ponintdos) {
		int newX = this.x + ponintdos.getPointX();
		int newY = this.y + ponintdos.getPointY();
		return new Point(newX, newY);
	}
	
	public int getPointX() {
		return this.x;
	}
	
	public int getPointY() {
		return this.y;
	}
		
}
